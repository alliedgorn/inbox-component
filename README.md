# React Inbox

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites
```
nodejs npm git
```

### Installing

Clone the project to your desired directory.

```
git clone git@bitbucket.org:alliedgorn/inbox-component.git
```

After you've cloned the project, next, download all dependencies by this command.

```
npm install
```
Wait until dependencies finish installing then run npm start to start the API

```
npm start
```

This will start up the application, then you can access with [http://localhost:3000](http://localhost:3000).

## Running the tests

```
npm run test
```

## Authors

* **Wutthikorn Kongprasopkij**


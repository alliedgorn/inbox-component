import React, { Component } from 'react';
import './App.css';
import GornMail from './GornMail';
import emailFetcher from './middleware/emailFetcher';

import 'font-awesome/css/font-awesome.css';

class App extends Component {
    render() {
        const WrappedGornMail = emailFetcher(GornMail);
        return (
            <div className="App">
                <WrappedGornMail />
            </div>
        );
    }
}

export default App;

import React, { Component } from 'react';
import Inbox from '../../components/Inbox';
import PropTypes from 'prop-types';

class GornMail extends Component {

    static propTypes = {
        getData: PropTypes.func.isRequired
    };

    constructor() {
        super();

        this.state = {
            emails: []
        };
    }

    componentDidMount() {
        this.props.getData().then((data) => {
            const emails = data.map(email => {
                email.selected = false;
                return email;
            });

            this.setState({
                emails: emails
            });
        });
    }

    toggleSelectEmail = (index) => {
        this.setState({
            emails: this.state.emails.map((email, i) => {
                if (i === index) {
                    email.selected = !email.selected;
                }
                return email;
            })
        })
    };

    render() {
        return (
            <div style={{ width: "500px", height: "600px" }}>
                <Inbox emails={this.state.emails} handleIconClick={this.toggleSelectEmail} />
            </div>
        );
    }
}

export default GornMail;
import React from 'react';
import { shallow } from 'enzyme';
import GornMail from '../';

describe('GornMail', () => {

    let props;
    beforeEach(() => {
        props = {
            getData: () => new Promise(() => {
            })
        }
    });

    it('renders without crashing', () => {
        let mounted = shallow(<GornMail {...props} />);
    });

    it('renders an inbox component', () => {
        let mounted = shallow(<GornMail {...props} />);
        const inbox = mounted.find('Inbox');
        expect(inbox.length).toBe(1);
    });
});
import React, { Component } from 'react';
import moment from 'moment';

export default (Wrapped) => {
    return class extends Component {
        getData = () => {
            return new Promise((resolve) => {
                setTimeout(() => {
                    resolve([{
                        "from": { "name": "Now TV", "email": "nowtv@test.com" },
                        "subject": "Grab another Pass, you need to be watchingthis...",
                        "body": "Oscar winners Sir Anthony Hopkins and Ed Harrisjoin an impressive cast boasting the likes of Thandie Newton,James Marsden and Jeffrey Wright.",
                        "sendDateTime": moment('2018-07-30 06:30:26.123')
                    }, {
                        "from": { "name": "Investopedia Terms", "email": "investopedia@test.com" },
                        "subject": "What is &#39;Fibonanci Retracement&#39;?",
                        "body": "Fibonacci retracement is a term used intechnical analysis that refers to areas of support (pricestops going lower) or resistance (price stops going higher).",
                        "sendDateTime": moment('2018-07-30 08:30:26.123')
                    }, {
                        "from": { "name": "ASICS Greater Manchester Marathon ", "email": "events@human-race.co.uk" },
                        "subject": "Your chance to take on the marathon",
                        "body": "Do you feel inspired to take on one of Europe&#39;smost highly regarded and popular marathons?",
                        "sendDateTime": moment('2018-07-30 12:30:26.123')
                    }, {
                        "from": { "name": "Now TV", "email": "nowtv@test.com" },
                        "subject": "Grab another Pass, you need to be watchingthis...",
                        "body": "Oscar winners Sir Anthony Hopkins and Ed Harrisjoin an impressive cast boasting the likes of Thandie Newton,James Marsden and Jeffrey Wright.",
                        "sendDateTime": moment('2018-07-30 06:30:26.123')
                    }, {
                        "from": { "name": "Investopedia Terms", "email": "investopedia@test.com" },
                        "subject": "What is &#39;Fibonanci Retracement&#39;?",
                        "body": "Fibonacci retracement is a term used intechnical analysis that refers to areas of support (pricestops going lower) or resistance (price stops going higher).",
                        "sendDateTime": moment('2018-07-30 08:30:26.123')
                    }, {
                        "from": { "name": "ASICS Greater Manchester Marathon ", "email": "events@human-race.co.uk" },
                        "subject": "Your chance to take on the marathon",
                        "body": "Do you feel inspired to take on one of Europe&#39;smost highly regarded and popular marathons?",
                        "sendDateTime": moment('2018-07-30 12:30:26.123')
                    }, {
                        "from": { "name": "Now TV", "email": "nowtv@test.com" },
                        "subject": "Grab another Pass, you need to be watchingthis...",
                        "body": "Oscar winners Sir Anthony Hopkins and Ed Harrisjoin an impressive cast boasting the likes of Thandie Newton,James Marsden and Jeffrey Wright.",
                        "sendDateTime": moment('2018-07-30 06:30:26.123')
                    }, {
                        "from": { "name": "Investopedia Terms", "email": "investopedia@test.com" },
                        "subject": "What is &#39;Fibonanci Retracement&#39;?",
                        "body": "Fibonacci retracement is a term used intechnical analysis that refers to areas of support (pricestops going lower) or resistance (price stops going higher).",
                        "sendDateTime": moment('2018-07-30 08:30:26.123')
                    }, {
                        "from": { "name": "ASICS Greater Manchester Marathon ", "email": "events@human-race.co.uk" },
                        "subject": "Your chance to take on the marathon",
                        "body": "Do you feel inspired to take on one of Europe&#39;smost highly regarded and popular marathons?",
                        "sendDateTime": moment('2018-07-30 12:30:26.123')
                    }, {
                        "from": { "name": "Now TV", "email": "nowtv@test.com" },
                        "subject": "Grab another Pass, you need to be watchingthis...",
                        "body": "Oscar winners Sir Anthony Hopkins and Ed Harrisjoin an impressive cast boasting the likes of Thandie Newton,James Marsden and Jeffrey Wright.",
                        "sendDateTime": moment('2018-07-30 06:30:26.123')
                    }, {
                        "from": { "name": "Investopedia Terms", "email": "investopedia@test.com" },
                        "subject": "What is &#39;Fibonanci Retracement&#39;?",
                        "body": "Fibonacci retracement is a term used intechnical analysis that refers to areas of support (pricestops going lower) or resistance (price stops going higher).",
                        "sendDateTime": moment('2018-07-30 08:30:26.123')
                    }, {
                        "from": { "name": "ASICS Greater Manchester Marathon ", "email": "events@human-race.co.uk" },
                        "subject": "Your chance to take on the marathon",
                        "body": "Do you feel inspired to take on one of Europe&#39;smost highly regarded and popular marathons?",
                        "sendDateTime": moment('2018-07-30 12:30:26.123')
                    }]);
                }, 2000);
            });
        };

        render() {
            return <Wrapped getData={this.getData} {...this.props} />
        }
    }

};
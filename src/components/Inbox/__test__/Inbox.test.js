import React from 'react';
import { shallow } from 'enzyme';
import Inbox from '../Inbox';

describe('Inbox', () => {

    let mounted;

    beforeEach(() => {
        mounted = shallow(<Inbox />);
    });

    it('renders without crashing', () => {
        let mounted = shallow(<Inbox />);
    });

    it('renders Header', () => {
        const filter = mounted.find('Header');
        expect(filter.length).toBe(1);
    });

    it('renders EmailList', () => {
        const filter = mounted.find('EmailList');
        expect(filter.length).toBe(1);
    });
});
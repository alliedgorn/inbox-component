import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Header from './components/Header';
import EmailList from './components/EmailList';
import MomentPropTypes from 'react-moment-proptypes';
import './Inbox.css';

class Inbox extends Component {

    static propTypes = {
        width: PropTypes.string,
        height: PropTypes.string,
        emails: PropTypes.arrayOf(
            PropTypes.shape({
                from: PropTypes.shape({
                    name: PropTypes.string.isRequired,
                    email: PropTypes.string.isRequired
                }),
                subject: PropTypes.string.isRequired,
                body: PropTypes.string.isRequired,
                sendDateTime: MomentPropTypes.momentObj,
                selected: PropTypes.bool.isRequired
            })
        ),
        handleFilterClick: PropTypes.func,
        handleEmailClick: PropTypes.func,
        handleIconClick: PropTypes.func,
        handleTrashCanIconClick: PropTypes.func,
        handleMailIconClick: PropTypes.func,
        handleFlagIconClick: PropTypes.func,
        handleBookmarkIconClick: PropTypes.func
    };

    static defaultProps = {
        width: "100%",
        height: "100%",
        emails: [],
        handleFilterClick: () => {
        },
        handleEmailClick: () => {
        },
        handleIconClick: () => {
        },
        handleTrashCanIconClick: () => {
        },
        handleMailIconClick: () => {
        },
        handleFlagIconClick: () => {
        },
        handleBookmarkIconClick: () => {
        }
    };

    render() {
        return (
            <div className={`Inbox`} style={{ width: this.props.width, height: this.props.height }}>
                <Header handleFilterClick={this.props.handleFilterClick} />
                <EmailList emails={this.props.emails}
                           handleEmailClick={this.props.handleEmailClick}
                           handleIconClick={this.props.handleIconClick}
                           handleTrashCanIconClick={this.props.handleTrashCanIconClick}
                           handleMailIconClick={this.props.handleMailIconClick}
                           handleFlagIconClick={this.props.handleFlagIconClick}
                           handleBookmarkIconClick={this.props.handleBookmarkIconClick} />
            </div>
        );
    }
}

export default Inbox;


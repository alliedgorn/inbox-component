import React, { Component } from 'react';
import Email from './Email/Email';
import PropTypes from 'prop-types';
import MomentPropTypes from 'react-moment-proptypes';

import './EmailList.css';

class EmailList extends Component {

    static propTypes = {
        emails: PropTypes.arrayOf(
            PropTypes.shape({
                from: PropTypes.shape({
                    name: PropTypes.string.isRequired,
                    email: PropTypes.string.isRequired
                }),
                subject: PropTypes.string.isRequired,
                body: PropTypes.string.isRequired,
                sendDateTime: MomentPropTypes.momentObj,
                selected: PropTypes.bool.isRequired
            })
        ),
        handleFilterClick: PropTypes.func,
        handleEmailClick: PropTypes.func,
        handleIconClick: PropTypes.func,
        handleTrashCanIconClick: PropTypes.func,
        handleMailIconClick: PropTypes.func,
        handleFlagIconClick: PropTypes.func,
        handleBookmarkIconClick: PropTypes.func
    };

    static defaultProps = {
        emails: []
    };

    render() {
        return (
            <div className={`EmailList`}>
                {this.props.emails.map((email, index) => <Email key={index} index={index} {...email}
                                                                handleClick={this.props.handleEmailClick}
                                                                handleIconClick={this.props.handleIconClick}
                                                                handleTrashCanIconClick={this.props.handleTrashCanIconClick}
                                                                handleMailIconClick={this.props.handleMailIconClick}
                                                                handleFlagIconClick={this.props.handleFlagIconClick}
                                                                handleBookmarkIconClick={this.props.handleBookmarkIconClick} />)}
            </div>
        );
    }
}

export default EmailList;
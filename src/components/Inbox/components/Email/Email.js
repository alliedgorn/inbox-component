import React, { Component } from 'react';
import PropTypes from 'prop-types';
import MomentPropTypes from 'react-moment-proptypes';
import moment from 'moment';
import './Email.css';
import Icon from './components/Icon';
import Tools from './components/Tools';

class Email extends Component {

    static propTypes = {
        index: PropTypes.number.isRequired,
        from: PropTypes.shape({
            email: PropTypes.string.isRequired,
            name: PropTypes.string.isRequired
        }),
        subject: PropTypes.string.isRequired,
        body: PropTypes.string.isRequired,
        sendDateTime: MomentPropTypes.momentObj,
        selected: PropTypes.bool,
        handleClick: PropTypes.func,
        handleIconClick: PropTypes.func,
        handleTrashCanIconClick: PropTypes.func,
        handleMailIconClick: PropTypes.func,
        handleFlagIconClick: PropTypes.func,
        handleBookmarkIconClick: PropTypes.func
    };

    static defaultProps = {
        selected: false
    };

    constructor() {
        super();

        this.state = {
            hovered: false
        };
    }

    renderSendDateTime() {
        if (moment().date() - this.props.sendDateTime.date() < 1) {
            return this.props.sendDateTime.format('hh:mm');
        }
        return this.props.sendDateTime.format('DD/MM/YYYY');

    }

    renderTools() {
        return (
            <div className={'tools'}>
                <Tools handleTrashCanIconClick={this._handleTrashCanIconClick}
                       handleMailIconClick={this._handleMailIconClick}
                       handleFlagIconClick={this._handleFlagIconClick}
                       handleBookmarkIconClick={this._handleBookmarkIconClick} />
            </div>
        );
    }

    hashCode(str) {
        let hash = 0;
        for (let i = 0; i < str.length; i++) {
            hash = str.charCodeAt(i) + ((hash << 5) - hash);
        }
        return hash;
    }

    intToRGB(i) {
        let c = (i & 0x00FFFFFF)
            .toString(16)
            .toUpperCase();

        return "00000".substring(0, 6 - c.length) + c;
    }

    onMouseEnter = () => {
        this.setState({ hovered: true });
    };

    onMouseLeave = () => {
        this.setState({ hovered: false });
    };

    _handleClick = () => {
        this.props.handleClick(this.props.index);
    };

    _handleIconClick = () => {
        this.props.handleIconClick(this.props.index);
    };

    _handleTrashCanIconClick = () => {
        this.props.handleTrashCanIconClick(this.props.index);
    };

    _handleMailIconClick = () => {
        this.props.handleMailIconClick(this.props.index);
    };

    _handleFlagIconClick = () => {
        this.props.handleFlagIconClick(this.props.index);
    };

    _handleBookmarkIconClick = () => {
        this.props.handleBookmarkIconClick(this.props.index);
    };


    render() {
        return (
            <div className={`Email${(this.props.selected ? ' selected' : '')}`}
                 onMouseEnter={this.onMouseEnter}
                 onMouseLeave={this.onMouseLeave}>
                <Icon
                    name={this.props.from.name}
                    color={`#${this.intToRGB(this.hashCode(this.props.from.email))}`}
                    checked={this.props.selected}
                    handleClick={this._handleIconClick} />
                <div className={'details'} onClick={this._handleClick}>
                    <div className={'sender'}>{this.props.from.name}</div>
                    {(this.state.hovered) && this.renderTools()}
                    <div className={'subject'}>{this.props.subject}</div>
                    <div className={'send-date-time'}>{this.renderSendDateTime()}</div>
                    <div className={'body'}>{this.props.body}</div>
                </div>
            </div>
        );
    }
}

export default Email;
import React from 'react';
import { shallow } from 'enzyme';
import Email from '../Email';
import moment from 'moment';
import MockDate from 'mockdate';

describe('Email', () => {

    let mounted;
    let props;

    beforeEach(() => {

        props = {
            index: 0,
            from: {
                name: 'Now TV',
                email: 'nowtv@test.com'
            },
            subject: 'Grab another Pass, you need to be watching this...',
            body: 'Oscar winners Sir Anthony Hopkins and Ed Harris join an impressive cast boasting the likes of Thandie Newton, James Marsden and Jeffrey Wright.',
            sendDateTime: moment('2013-02-08 09:30:26.123'),
            selected: false
        };

        mounted = shallow(<Email {...props} />);
    });

    it('renders without crashing', () => {
        let mounted = shallow(<Email {...props} />);
    });

    it('should not render tools', () => {
        const tools = mounted.find('div.tools');
        expect(tools.length).toBe(0);
    });

    it('renders sender with correct text', () => {
        const sender = mounted.find('div.sender');
        expect(sender.length).toBe(1);
        expect(sender.text()).toEqual('Now TV');
    });

    it('renders subject with correct text', () => {
        const subject = mounted.find('div.subject');
        expect(subject.length).toBe(1);
        expect(subject.text()).toEqual('Grab another Pass, you need to be watching this...');
    });

    it('renders body with correct text', () => {
        const body = mounted.find('div.body');
        expect(body.length).toBe(1);
        expect(body.text()).toEqual('Oscar winners Sir Anthony Hopkins and Ed Harris join an impressive cast boasting the likes of Thandie Newton, James Marsden and Jeffrey Wright.');
    });

    it('renders Icon', () => {
        const icon = mounted.find('Icon');
        expect(icon.length).toBe(1);
    });

    it('call a function passed to it when details section clicked', () => {
        const mockCallBack = jest.fn();
        const mountedWithCallback = shallow(<Email {...props} handleClick={mockCallBack} />);
        mountedWithCallback.find('div.details').simulate('click');
        expect(mockCallBack.mock.calls.length).toEqual(1);
    });

    it('has toggle hover state on mouse over and mouse out', () => {
        mounted.simulate('mouseEnter');
        expect(mounted.state()).toHaveProperty('hovered', true);
        mounted.simulate('mouseLeave');
        expect(mounted.state()).toHaveProperty('hovered', false);
    });

});

describe('When send date is on the same day', () => {

    let mounted;
    let props;

    beforeEach(() => {

        props = {
            index: 0,
            from: {
                name: 'Now TV',
                email: 'nowtv@test.com'
            },
            subject: 'Grab another Pass, you need to be watching this...',
            body: 'Oscar winners Sir Anthony Hopkins and Ed Harris join an impressive cast boasting the likes of Thandie Newton, James Marsden and Jeffrey Wright.',
            sendDateTime: moment('2018-02-15 12:33:23'),
            selected: true
        };

        MockDate.set('2018-02-15');

        mounted = shallow(<Email {...props} />);
    });

    it('renders correct send date', () => {
        const sendDateTime = mounted.find('div.send-date-time');
        expect(sendDateTime.length).toBe(1);
        expect(sendDateTime.text()).toEqual('12:33');
    });

});

describe('When send date is not on the same day', () => {

    let mounted;
    let props;

    beforeEach(() => {

        props = {
            index: 0,
            from: {
                name: 'Now TV',
                email: 'nowtv@test.com'
            },
            subject: 'Grab another Pass, you needq to be watching this...',
            body: 'Oscar winners Sir Anthony Hopkins and Ed Harris join an impressive cast boasting the likes of Thandie Newton, James Marsden and Jeffrey Wright.',
            sendDateTime: moment('2018-02-15 12:33:23'),
            selected: true
        };

        MockDate.set('2018-02-16');

        mounted = shallow(<Email {...props} />);
    });

    it('renders correct send date', () => {
        const sendDateTime = mounted.find('div.send-date-time');
        expect(sendDateTime.length).toBe(1);
        expect(sendDateTime.text()).toEqual('15/02/2018');
    });

});
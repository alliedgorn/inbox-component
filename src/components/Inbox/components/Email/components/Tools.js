import React, { Component } from 'react';
import PropTypes from 'prop-types';
import FontAwesomeIcon from 'react-fontawesome';
import './Tools.css';

class Tools extends Component {

    static propTypes = {
        handleTrashCanIconClick: PropTypes.func,
        handleMailIconClick: PropTypes.func,
        handleFlagIconClick: PropTypes.func,
        handleBookmarkIconClick: PropTypes.func
    };

    static defaultProps = {};

    renderTrashCan() {
        return (
            <FontAwesomeIcon className={`tool trash-can`} name={`trash-o`}
                             onClick={this.props.handleTrashCanIconClick} />
        );
    }

    renderMail() {
        return (
            <FontAwesomeIcon className={`tool mail`} name={`envelope-o`} onClick={this.props.handleMailIconClick} />
        );
    }

    renderFlag() {
        return (
            <FontAwesomeIcon className={`tool flag`} name={`flag-o`} onClick={this.props.handleFlagIconClick} />
        );
    }

    renderBookmark() {
        return (
            <FontAwesomeIcon className={`tool bookmark`} name={`bookmark-o`}
                             onClick={this.props.handleBookmarkIconClick} />
        );
    }

    render() {
        return (
            <div className={'Tools'}>
                {this.props.handleTrashCanIconClick && this.renderTrashCan()}
                {this.props.handleMailIconClick && this.renderMail()}
                {this.props.handleFlagIconClick && this.renderFlag()}
                {this.props.handleBookmarkIconClick && this.renderBookmark()}
            </div>
        );
    }
}

export default Tools;
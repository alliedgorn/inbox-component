import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './Icon.css';

class Icon extends Component {

    static propTypes = {
        name: PropTypes.string.isRequired,
        handleClick: PropTypes.func,
        checked: PropTypes.bool,
        color: PropTypes.string
    };

    static defaultProps = {
        color: "#AAAAAA",
        checked: false,
        handleClick: () => {
        }
    };

    generateAbbr() {
        const nameParts = this.props.name.split(/ /).map(name => name.charAt(0));
        if (nameParts.length >= 2) {
            return `${nameParts[0]}${nameParts[1]}`;
        }
        return nameParts[0];
    }

    renderAbbr() {
        return (
            <div className={'abbr-holder trigger'} style={{ backgroundColor: this.props.color }}
                 onClick={this.props.handleClick}>
                <span className={'abbr-text'}>{this.generateAbbr(this.props.name)}</span>
            </div>
        );
    }

    renderCheckedIcon() {
        return (
            <div className={'checked trigger'} onClick={this.props.handleClick}><span /></div>
        );
    }

    render() {
        const { checked } = this.props;
        return (
            <div className={'Icon'}>
                {!checked && this.renderAbbr()}
                {checked && this.renderCheckedIcon()}
            </div>
        );
    }
}

export default Icon;
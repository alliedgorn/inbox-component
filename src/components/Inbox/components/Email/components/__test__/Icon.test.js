import React from 'react';
import { shallow } from 'enzyme';
import Icon from '../Icon';

describe('Icon', () => {

    let mounted;
    let props;

    beforeEach(() => {
        props = {
            name: "Abdul Ja"
        };
        mounted = shallow(<Icon {...props} />);
    });

    it('renders without crashing', () => {
        let mounted = shallow(<Icon {...props} />);
    });

    it('renders name abbreviation', () => {
        let abbr = mounted.find('div.abbr-holder');
        let abbrText = abbr.find('span.abbr-text');
        expect(abbrText.text()).toEqual("AJ");
    });

    it('call a function passed to it when clicked', () => {
        const mockCallBack = jest.fn();
        const mountedWithCallback = shallow(<Icon {...props} handleClick={mockCallBack} />);
        mountedWithCallback.find('div.trigger').simulate('click');
        expect(mockCallBack.mock.calls.length).toEqual(1);
    });

});

describe('When checked', () => {

    let mounted;
    let props;

    beforeEach(() => {
        props = {
            name: "Abdul Ja",
            checked: true
        };
        mounted = shallow(<Icon {...props} />);
    });

    it('renders without crashing', () => {
        let mounted = shallow(<Icon {...props} />);
    });

    it('renders checked', () => {
        let checked = mounted.find('div.checked');
        expect(checked.length).toBe(1);
    });

});
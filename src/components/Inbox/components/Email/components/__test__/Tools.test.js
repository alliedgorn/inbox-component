import React from 'react';
import { shallow } from 'enzyme';
import Tools from '../Tools';

describe('Tools', () => {

    let mounted;
    let props;

    beforeEach(() => {
        props = {};
        mounted = shallow(<Tools {...props} />);
    });

    it('renders without crashing', () => {
        let mounted = shallow(<Tools {...props} />);
    });

});

describe('When handleTrashCanIconClick was passed', () => {
    let mounted;
    let props;
    const mockCallBack = jest.fn();

    beforeEach(() => {
        props = {
            handleTrashCanIconClick: mockCallBack
        };
        mounted = shallow(<Tools {...props} />);
    });

    it('renders without crashing', () => {
        let mounted = shallow(<Tools {...props} />);
    });

    it('renders trash can', () => {
        const trashCan = mounted.find('.trash-can');
        expect(trashCan.length).toBe(1);
    });

    it('calls handleTrashCanIconClick when trash can was clicked', () => {
        const trashCan = mounted.find('.trash-can');
        trashCan.simulate('click');
        expect(mockCallBack.mock.calls.length).toEqual(1);
    });
});

describe('When handleMailIconClick was passed', () => {
    let mounted;
    let props;
    const mockCallBack = jest.fn();

    beforeEach(() => {
        props = {
            handleMailIconClick: mockCallBack
        };
        mounted = shallow(<Tools {...props} />);
    });

    it('renders without crashing', () => {
        let mounted = shallow(<Tools {...props} />);
    });

    it('renders mail icon', () => {
        const mail = mounted.find('.mail');
        expect(mail.length).toBe(1);
    });

    it('calls handleMailIconClick when mail icon was clicked', () => {
        const mail = mounted.find('.mail');
        mail.simulate('click');
        expect(mockCallBack.mock.calls.length).toEqual(1);
    });
});

describe('When handleFlagIconClick was passed', () => {
    let mounted;
    let props;
    const mockCallBack = jest.fn();

    beforeEach(() => {
        props = {
            handleFlagIconClick: mockCallBack
        };
        mounted = shallow(<Tools {...props} />);
    });

    it('renders without crashing', () => {
        let mounted = shallow(<Tools {...props} />);
    });

    it('renders trash can', () => {
        const flag = mounted.find('.flag');
        expect(flag.length).toBe(1);
    });

    it('calls handleFlagIconClick when flag icon was clicked', () => {
        const flag = mounted.find('.flag');
        flag.simulate('click');
        expect(mockCallBack.mock.calls.length).toEqual(1);
    });
});

describe('When handleBookmarkIconClick was passed', () => {
    let mounted;
    let props;
    const mockCallBack = jest.fn();

    beforeEach(() => {
        props = {
            handleBookmarkIconClick: mockCallBack
        };
        mounted = shallow(<Tools {...props} />);
    });

    it('renders without crashing', () => {
        let mounted = shallow(<Tools {...props} />);
    });

    it('renders bookmark icon', () => {
        const bookmark = mounted.find('.bookmark');
        expect(bookmark.length).toBe(1);
    });

    it('calls handleBookmarkIconClick when bookmark icon was clicked', () => {
        const bookmark = mounted.find('.bookmark');
        bookmark.simulate('click');
        expect(mockCallBack.mock.calls.length).toEqual(1);
    });
});

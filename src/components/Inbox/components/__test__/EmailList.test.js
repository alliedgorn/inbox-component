import React from 'react';
import { shallow } from 'enzyme';
import EmailList from '../EmailList';
import moment from 'moment';

describe('EmailList', () => {

    let mounted;
    let props;

    beforeEach(() => {
        props = {
            emails: [
                {
                    "id": 1,
                    "from": {
                        "name": "Now TV",
                        "email": "nowtv@test.com"
                    },
                    "subject": "Grab another Pass, you need to be watching this...",
                    "body": "Oscar winners Sir Anthony Hopkins and Ed Harris join an impressive cast boasting the likes of Thandie Newton, James Marsden and Jeffrey Wright.",
                    "sendDateTime": moment('2018-07-30 09:30:26.123'),
                    "selected": true
                },
                {
                    "id": 2,
                    "from": {
                        "name": "Investopedia Terms",
                        "email": "investopedia@test.com"
                    },
                    "subject": "What is &#39;Fibonanci Retracement&#39;?",
                    "body": "Fibonacci retracement is a term used in technical analysis that refers to areas of support (price stops going lower) or resistance (price stops going higher).",
                    "sendDateTime": moment('2018-07-30 07:30:26.123'),
                    "selected": false
                },
                {
                    "id": 3,
                    "from": {
                        "name": "ASICS Greater Manchester Marathon ",
                        "email": "events@human-race.co.uk"
                    },
                    "subject": "Your chance to take on the marathon",
                    "body": "Do you feel inspired to take on one of Europe'sZ most highly regarded and popular marathons?",
                    "sendDateTime": moment('2018-07-30 06:30:26.123'),
                    "selected": false
                }
            ]
        };
        mounted = shallow(<EmailList {...props} />);
    });

    it('renders without crashing', () => {
        let mounted = shallow(<EmailList {...props} />);
    });

    it('renders Emails', () => {
        const filter = mounted.find('Email');
        expect(filter.length).toBe(3);
    });

});
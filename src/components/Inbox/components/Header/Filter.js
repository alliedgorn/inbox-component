import React, { Component } from 'react';
import PropTypes from 'prop-types';
import FontAwesome from 'react-fontawesome';

class Filter extends Component {

    static propTypes = {
        handleFilterClick: PropTypes.func
    };

    static defaultProps = {
        handleFilterClick: () => {
        }
    };

    render() {
        return (
            <span className={`filter link`}>Filter <FontAwesome name={`chevron-down`} /></span>
        );
    }
}

export default Filter;
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Filter from './Filter';

import './Header.css';

class Header extends Component {

    static propTypes = {
        title: PropTypes.string,
        handleFilterClick: PropTypes.func
    };

    static defaultProps = {
        title: "Inbox",
        handleFilterClick: () => {
        }
    };

    render() {
        return (
            <div className={`Header`}>
                <span className={`title`}>{this.props.title}</span><Filter
                handleFilterClick={this.props.handleFilterClick} />
            </div>
        );
    }
}

export default Header;
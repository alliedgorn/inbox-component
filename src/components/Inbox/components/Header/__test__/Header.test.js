import React from 'react';
import { shallow } from 'enzyme';
import Header from '../Header';

describe('Header', () => {

    let mounted;
    let props;

    beforeEach(() => {
        props = {
            title: "Inbox"
        };
        mounted = shallow(<Header {...props} />);
    });

    it('renders without crashing', () => {
        let mounted = shallow(<Header {...props} />);
    });

    it('renders a Filter', () => {
        const filter = mounted.find('Filter');
        expect(filter.length).toBe(1);
    });

    it('renders a title span with correct text', () => {
        const span = mounted.find('span.title');
        expect(span.length).toBe(1);
        expect(span.text()).toEqual(props.title);
    });
});
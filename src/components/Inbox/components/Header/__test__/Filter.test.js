import React from 'react';
import { shallow } from 'enzyme';
import Filter from '../Filter';

describe('Filter', () => {

    let mounted;
    let props;
    let mockCallBack;

    beforeEach(() => {
        mockCallBack = jest.fn();
        props = {
            handleFilterClick: mockCallBack
        };
        mounted = shallow(<Filter />);
    });

    it('renders without crashing', () => {
        let mounted = shallow(<Filter />);
    });

    it('renders filter element', () => {
        const filter = mounted.find('span.filter');
        expect(filter.length).toBe(1);
    });

    it('call handleFilterClick when filter was clicked', () => {
        const filter = mounted.find('span.filter');
        filter.simulate('click');
        expect(filter.length).toBe(1);
    });
});